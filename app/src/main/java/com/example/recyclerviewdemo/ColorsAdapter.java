package com.example.recyclerviewdemo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerviewdemo.databinding.RecyclerViewColorBinding;

import java.util.ArrayList;

public class ColorsAdapter extends RecyclerView.Adapter<ColorsAdapter.ColorViewHolder> {
    private ArrayList<Color> colors;
    private MainActivity mainActivity;

    public ColorsAdapter(ArrayList<Color> colors, MainActivity mainActivity) {
        this.colors = colors;
        this.mainActivity = mainActivity;
    }

    @NonNull
    @Override
    public ColorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Instancier notre ColorViewHolder, par exemple avec un inflate de layout
        RecyclerViewColorBinding binding = RecyclerViewColorBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);

        return new ColorViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorViewHolder holder, int position) {
        // Est appele chaque fois que le RecyclerView doit afficher une cellule
        // Il nous fourni 1 ViewHolder et la position a afficher
        // On doit donc configurer le ViewHolder pour l'item correspondant de la source de donnees

        Color currentColor = colors.get(position);
        holder.bind(currentColor);
    }

    @Override
    public int getItemCount() {
        // Indique au RecyclerView combien d'items il doit afficher
        return colors.size();
    }

    public class ColorViewHolder extends RecyclerView.ViewHolder {
        private RecyclerViewColorBinding binding;

        public ColorViewHolder(@NonNull RecyclerViewColorBinding binding) {
            // Pour initialiser un ViewHolder, il faut fournir comment il s'affiche, sa View
            super(binding.getRoot()); // Car on utilise le binding, il faut envoyer la View racine

            this.binding = binding;
        }

        public void bind(final Color color) {
            // On delegue la responsabilite de configurer l'affichage au ViewHolder
            binding.viewColor.setBackgroundColor(color.getValue());
            binding.textName.setText(color.getName());

            // itemView represente la racine de la cellule
            // Penser a ajouter un background sur la vue racine du layout d'une cellule
            // android:background="?android:attr/selectableItemBackground" dans layouts/recycler_view_color.xml
            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // On ne veut PAS que l'adapter traite le clic
                    // Il doit deleguer le travail au controller, dans ce cas MainActivity
                    mainActivity.onColorClicked(color);
                }
            });
        }
    }
}
