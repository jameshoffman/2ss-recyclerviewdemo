package com.example.recyclerviewdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ColorsAdapter colorsAdapter;
    private ArrayList<Color> colors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //
        // Source de donnees
        //

        colors = new ArrayList();
        colors.add(new Color("Magenta", android.graphics.Color.MAGENTA));
        colors.add(new Color("Red", android.graphics.Color.RED));
        colors.add(new Color("Yellow", android.graphics.Color.YELLOW));
        colors.add(new Color("Green", android.graphics.Color.GREEN));
        colors.add(new Color("Cyan", android.graphics.Color.CYAN));
        colors.add(new Color("Blue", android.graphics.Color.BLUE));
        colors.add(new Color("Gray", android.graphics.Color.DKGRAY));
        colors.add(new Color("Black", android.graphics.Color.BLACK));

        //
        // Configuration du RecyclerView
        //

        RecyclerView recyclerView = findViewById(R.id.recyclerView); // ou view binding
        recyclerView.setHasFixedSize(true); // Optimisation si toutes les cellules ont la meme taille
        // Ajoute une ligne separatrice entre 2 cellules verticales ou horizontales, DividerItemDecoration.HORIZONTAL
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        recyclerView.setLayoutManager(new LinearLayoutManager(this));//
        // Par exemple, pour un defilement horizontal
        // new LinearLayoutManager(this, RecyclerView.HORIZONTAL, boolean isReversed)

        // L'adapter recoit les donnees a afficher
        // On lui envoie egalement this pour qu'il puisse communiquer
        // avec l'activity lors d'evenement
        colorsAdapter = new ColorsAdapter(colors, this);
        recyclerView.setAdapter(colorsAdapter);


        //
        // Modification de la liste
        //
        Button deleteButton = findViewById(R.id.buttonDelete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (colors.size() > 0) {
                    Color deleted = colors.remove(0);
                    Toast.makeText(MainActivity.this, "Deleted " + deleted.getName(), Toast.LENGTH_SHORT).show();

                    // On doit informer l'adapter que les donnees ont changees
                    colorsAdapter.notifyDataSetChanged();
                    // On pourrait donner plus de details pour optimiser l'affichage
                    // colorsAdapter.notifyItemRemoved(0);
                    // Il existe des variantes pour l'insertion, la mise a jour, la position

                    // Ici on partage la reference vers l'array colors
                    // Si ce n'etait pas le cas il faudrait envoyer le nouveau tableau a l'adapter
                    // ex: colorsAdapter.setColors(newColors);
                    // et creer le setter correspondant dans l'adapter
                    // puis appeler .notifyXYZ()
                } else {
                    Toast.makeText(MainActivity.this, "Nothing to delete...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onColorClicked(Color color) {
        Toast.makeText(MainActivity.this, "Clicked " + color.getName(), Toast.LENGTH_SHORT).show();
    }
}